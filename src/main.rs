pub mod mcts {
    extern crate rand;
    use rand::prelude::*;
    use std::collections::HashMap;
    use std::hash::Hash;
    pub enum State<P1State, P2State> {
        First(P1State),
        Second(P2State),
        Endgame { p1_reward: f32, p2_reward: f32 },
    }

    #[derive(Debug)]
    pub enum MoveError {
        BadMove,
        OtherPlayerTurn,
    }

    #[derive(Clone, Copy)]
    pub enum Move<P1M: Copy, P2M: Copy> {
        P1Move(P1M),
        P2Move(P2M),
    }

    impl<M1: Copy, M2: Copy> Move<&M1, &M2> {
        fn copy_ref(&self) -> Move<M1, M2> {
            match self {
                Move::P1Move(x) => Move::P1Move(**x),
                Move::P2Move(x) => Move::P2Move(**x),
            }
        }
    }

    pub trait GameState<Move>: Eq {
        fn possible_moves(&self) -> Vec<Move>;
    }
    pub trait Game
    where
        Self: Sized,
    {
        type P1Move: Eq + Hash + Copy;
        type P2Move: Eq + Hash + Copy;
        type P1State: GameState<Self::P1Move>;
        type P2State: GameState<Self::P2Move>;
        fn state(&self) -> State<Self::P1State, Self::P2State>;
        fn make_p1_move(&self, mv: &Self::P1Move) -> Result<Self, MoveError>;
        fn make_p2_move(&self, mv: &Self::P2Move) -> Result<Self, MoveError>;
        fn is_finished(&self) -> bool {
            match self.state() {
                State::Endgame { .. } => true,
                _ => false,
            }
        }

        fn make_move_choose(
            &self,
            mv: &Move<Self::P1Move, Self::P2Move>,
        ) -> Result<Self, MoveError> {
            match mv {
                Move::P1Move(m) => self.make_p1_move(m),
                Move::P2Move(m) => self.make_p2_move(m),
            }
        }

        fn make_move_choose_r(
            &self,
            mv: Move<&Self::P1Move, &Self::P2Move>,
        ) -> Result<Self, MoveError> {
            match mv {
                Move::P1Move(m) => self.make_p1_move(m),
                Move::P2Move(m) => self.make_p2_move(m),
            }
        }
    }

    //TODO rust does not quarantee TCO
    pub fn random_playout<G: Game, R: Rng>(game: G, rng: &mut R) -> (f32, f32) {
        match game.state() {
            State::Endgame {
                p1_reward,
                p2_reward,
            } => (p1_reward, p2_reward),
            State::First(state) => random_playout(
                game.make_p1_move(state.possible_moves().choose(rng).unwrap())
                    .unwrap(),
                rng,
            ),
            State::Second(state) => random_playout(
                game.make_p2_move(state.possible_moves().choose(rng).unwrap())
                    .unwrap(),
                rng,
            ),
        }
    }

    pub enum MctsNode<G: Game> {
        P1Node {
            state: G::P1State,
            qsum_p1: f32,
            qsum_p2: f32,
            visits: u32,
            successors: HashMap<G::P1Move, MctsNode<G>>,
        },
        P2Node {
            state: G::P2State,
            qsum_p1: f32,
            qsum_p2: f32,
            visits: u32,
            successors: HashMap<G::P2Move, MctsNode<G>>,
        },
        Leaf {
            qsum_p1: f32,
            qsum_p2: f32,
            visits: u32,
        },
    }

    pub enum SelResult<'a, G: Game> {
        Selection(Move<&'a G::P1Move, &'a G::P2Move>, &'a mut MctsNode<G>),
        Expansion(Move<&'a G::P1Move, &'a G::P2Move>, &'a mut MctsNode<G>),
    }

    impl<'a, G: Game> MctsNode<G> {
        pub fn new(game: &G) -> MctsNode<G> {
            match game.state() {
                State::First(s) => MctsNode::P1Node {
                    state: s,
                    qsum_p1: 0.0,
                    qsum_p2: 0.0,
                    visits: 0,
                    successors: HashMap::new(),
                },
                State::Second(s) => MctsNode::P2Node {
                    state: s,
                    qsum_p1: 0.0,
                    qsum_p2: 0.0,
                    visits: 0,
                    successors: HashMap::new(),
                },
                State::Endgame { .. } => MctsNode::Leaf {
                    visits: 0,
                    qsum_p1: 0.0,
                    qsum_p2: 0.0,
                },
            }
        }

        pub fn visits(&self) -> u32 {
            match self {
                MctsNode::P1Node { visits, .. } => *visits,
                MctsNode::P2Node { visits, .. } => *visits,
                MctsNode::Leaf { visits, .. } => *visits,
            }
        }

        pub fn qsum_p1(&self) -> f32 {
            match self {
                MctsNode::P1Node { qsum_p1, .. } => *qsum_p1,
                MctsNode::P2Node { qsum_p1, .. } => *qsum_p1,
                MctsNode::Leaf { qsum_p1, .. } => *qsum_p1,
            }
        }

        pub fn qsum_p2(&self) -> f32 {
            match self {
                MctsNode::P1Node { qsum_p2, .. } => *qsum_p2,
                MctsNode::P2Node { qsum_p2, .. } => *qsum_p2,
                MctsNode::Leaf { qsum_p2, .. } => *qsum_p2,
            }
        }

        pub fn visits_mut(&mut self) -> &mut u32 {
            match self {
                MctsNode::P1Node { visits, .. } => visits,
                MctsNode::P2Node { visits, .. } => visits,
                MctsNode::Leaf { visits, .. } => visits,
            }
        }

        pub fn qsum_p1_mut(&mut self) -> &mut f32 {
            match self {
                MctsNode::P1Node { qsum_p1, .. } => qsum_p1,
                MctsNode::P2Node { qsum_p1, .. } => qsum_p1,
                MctsNode::Leaf { qsum_p1, .. } => qsum_p1,
            }
        }

        pub fn qsum_p2_mut(&mut self) -> &mut f32 {
            match self {
                MctsNode::P1Node { qsum_p2, .. } => qsum_p2,
                MctsNode::P2Node { qsum_p2, .. } => qsum_p2,
                MctsNode::Leaf { qsum_p2, .. } => qsum_p2,
            }
        }

        pub fn successor(&mut self, mv: &Move<G::P1Move, G::P2Move>) -> Option<&mut MctsNode<G>> {
            match (mv, self) {
                (Move::P1Move(m), MctsNode::P1Node { successors, .. }) => successors.get_mut(m),
                (Move::P2Move(m), MctsNode::P2Node { successors, .. }) => successors.get_mut(m),
                _ => None,
            }
        }

        pub fn add_result(&mut self) {
            unimplemented!();
        }

        fn ucb1_formula(all_visits: u32, visits: u32, qsum: f32) -> f32 {
            qsum / visits as f32 + 1.41 * ((all_visits as f32).ln() / (visits as f32)).sqrt()
        }

        fn sel_inner_p1<M: Eq + Hash + Copy, S: GameState<M>>(
            state: &S,
            successors: &'a mut std::collections::HashMap<M, MctsNode<G>>,
            visits: u32,
        ) -> Option<(&'a M, &'a mut MctsNode<G>)> {
            if state.possible_moves().len() > successors.len() {
                None
            } else {
                successors.iter_mut().max_by(|i1, i2| {
                    Self::ucb1_formula(visits, i1.1.visits(), i1.1.qsum_p1())
                        .partial_cmp(&Self::ucb1_formula(visits, i2.1.visits(), i2.1.qsum_p1()))
                        .unwrap()
                })
            }
        }

        fn sel_inner_p2<M: Eq + Hash + Copy, S: GameState<M>>(
            state: &S,
            successors: &'a mut std::collections::HashMap<M, MctsNode<G>>,
            visits: u32,
        ) -> Option<(&'a M, &'a mut MctsNode<G>)> {
            if state.possible_moves().len() > successors.len() {
                None
            } else {
                successors.iter_mut().max_by(|i1, i2| {
                    Self::ucb1_formula(visits, i1.1.visits(), i1.1.qsum_p2())
                        .partial_cmp(&Self::ucb1_formula(visits, i2.1.visits(), i2.1.qsum_p2()))
                        .unwrap()
                })
            }
        }

        pub fn selection(
            &'a mut self,
        ) -> Option<(Move<&'a G::P1Move, &'a G::P2Move>, &'a mut MctsNode<G>)> {
            match self {
                MctsNode::P1Node {
                    state,
                    visits,
                    successors,
                    ..
                } => {
                    Self::sel_inner_p1(state, successors, *visits).map(|x| (Move::P1Move(x.0), x.1))
                }
                MctsNode::P2Node {
                    state,
                    visits,
                    successors,
                    ..
                } => {
                    Self::sel_inner_p2(state, successors, *visits).map(|x| (Move::P2Move(x.0), x.1))
                }
                MctsNode::Leaf { .. } => None,
            }
        }

        pub fn expansion<R: Rng>(
            &'a mut self,
            rng: &mut R,
            game: &G,
        ) -> Option<(Move<G::P1Move, G::P2Move>, &'a mut MctsNode<G>)> {
            match (self, game.state()) {
                (
                    MctsNode::P1Node {
                        state, successors, ..
                    },
                    State::First(st),
                ) if &st == state => {
                    if successors.len() == state.possible_moves().len() {
                        None
                    } else {
                        let m = *state
                            .possible_moves()
                            .iter()
                            .filter(|&x| !successors.contains_key(x))
                            .choose(rng)
                            .unwrap();
                        let next_gs = game.make_p1_move(&m);
                        match next_gs {
                            Result::Ok(gs) => {
                                let node = MctsNode::new(&gs);
                                successors.insert(m, node);
                                Some((Move::P1Move(m), successors.get_mut(&m).unwrap()))
                            }
                            _ => None,
                        }
                    }
                }
                (
                    MctsNode::P2Node {
                        state, successors, ..
                    },
                    State::Second(st),
                ) if &st == state => {
                    if successors.len() == state.possible_moves().len() {
                        None
                    } else {
                        let m = *state
                            .possible_moves()
                            .iter()
                            .filter(|&x| !successors.contains_key(x))
                            .choose(rng)
                            .unwrap();
                        let next_gs = game.make_p2_move(&m);
                        match next_gs {
                            Result::Ok(gs) => {
                                let node = MctsNode::new(&gs);
                                successors.insert(m, node);
                                Some((Move::P2Move(m), successors.get_mut(&m).unwrap()))
                            }
                            _ => None,
                        }
                    }
                }
                _ => None,
            }
        }
    }

    pub fn mcts_iteration<G: Game + Clone, R: Rng>(game: G, rng: &mut R, node: &mut MctsNode<G>) {
        let mut path: Vec<Move<G::P1Move, G::P2Move>> = Vec::new();

        fn sel<G: Game, R: Rng>(
            curr: &mut MctsNode<G>,
            path: &mut Vec<Move<G::P1Move, G::P2Move>>,
            g: G,
            rng: &mut R,
        ) -> (f32, f32) {
            match curr.selection() {
                Some((m, nn)) => {
                    path.push(m.copy_ref());
                    sel(
                        nn,
                        path,
                        {
                            match m {
                                Move::P1Move(m) => g.make_p1_move(&m).unwrap(),
                                Move::P2Move(m) => g.make_p2_move(&m).unwrap(),
                            }
                        },
                        rng,
                    )
                }
                None => match curr.expansion(rng, &g) {
                    Some((m, _)) => {
                        path.push(m);
                        random_playout(g.make_move_choose(&m).unwrap(), rng)
                    }
                    None => random_playout(g, rng),
                },
            }
        }

        let (p1, p2) = sel(node, &mut path, game.clone(), rng);

        let mut nn = node;
        for m in path {
            *nn.qsum_p1_mut() += p1;
            *nn.qsum_p2_mut() += p2;
            *nn.visits_mut() += 1;
            nn = nn.successor(&m).unwrap()
        }
        *nn.qsum_p1_mut() += p1;
        *nn.qsum_p2_mut() += p2;
        *nn.visits_mut() += 1;
    }
}

pub mod ttt {
    use crate::mcts::{Game, GameState, MoveError, State};

    #[derive(Copy, Clone, Hash, PartialEq, Eq)]
    pub enum Field {
        X,
        O,
        Empty,
    }

    #[derive(Copy, Clone, Hash, PartialEq, Eq, Debug)]
    pub enum Coord {
        A = 0,
        B = 1,
        C = 2,
    }

    #[derive(Copy, Clone, Hash, PartialEq, Eq)]
    pub struct TicTacToe(pub [Field; 9]);

    static ALL_COORDS: [(Coord, Coord); 9] = {
        use Coord::*;
        [
            (A, A),
            (A, B),
            (A, C),
            (B, A),
            (B, B),
            (B, C),
            (C, A),
            (C, B),
            (C, C),
        ]
    };

    pub static SINGLE_COORD: [Coord; 3] = [Coord::A, Coord::B, Coord::C];

    impl GameState<(Coord, Coord)> for TicTacToe {
        fn possible_moves(&self) -> Vec<(Coord, Coord)> {
            match self {
                TicTacToe(arr) => ALL_COORDS
                    .iter()
                    .filter(|x| arr[x.0 as usize * 3 + x.1 as usize] == Field::Empty)
                    .cloned()
                    .collect(),
            }
        }
    }

    use Coord::*;

    impl TicTacToe {
        pub const fn new() -> TicTacToe {
            TicTacToe([
                Field::Empty,
                Field::Empty,
                Field::Empty,
                Field::Empty,
                Field::Empty,
                Field::Empty,
                Field::Empty,
                Field::Empty,
                Field::Empty,
            ])
        }
        pub const fn coord_to_idx(r: Coord, c: Coord) -> usize {
            r as usize * 3 + c as usize
        }
        fn check_row(&self, r: Coord) -> bool {
            self.0[Self::coord_to_idx(r, A)] != Field::Empty
                && self.0[Self::coord_to_idx(r, A)] == self.0[Self::coord_to_idx(r, B)]
                && self.0[Self::coord_to_idx(r, B)] == self.0[Self::coord_to_idx(r, C)]
        }

        fn check_col(&self, c: Coord) -> bool {
            self.0[Self::coord_to_idx(A, c)] != Field::Empty
                && self.0[Self::coord_to_idx(A, c)] == self.0[Self::coord_to_idx(B, c)]
                && self.0[Self::coord_to_idx(B, c)] == self.0[Self::coord_to_idx(C, c)]
        }

        fn check_diags(&self) -> bool {
            self.0[Self::coord_to_idx(B, B)] != Field::Empty
                && ((self.0[Self::coord_to_idx(A, A)] == self.0[Self::coord_to_idx(B, B)]
                    && self.0[Self::coord_to_idx(C, C)] == self.0[Self::coord_to_idx(B, B)])
                    || (self.0[Self::coord_to_idx(A, C)] == self.0[Self::coord_to_idx(B, B)]
                        && self.0[Self::coord_to_idx(C, A)] == self.0[Self::coord_to_idx(B, B)]))
        }

        pub fn check_end_conditions(&self) -> Field {
            for i in &SINGLE_COORD {
                if self.check_row(*i) {
                    return self.0[Self::coord_to_idx(*i, A)];
                }
                if self.check_col(*i) {
                    return self.0[Self::coord_to_idx(A, *i)];
                }
            }
            if self.check_diags() {
                self.0[Self::coord_to_idx(B, B)]
            } else {
                Field::Empty
            }
        }
    }
    impl Game for TicTacToe {
        type P1Move = (Coord, Coord);
        type P2Move = (Coord, Coord);
        type P1State = Self;
        type P2State = Self;

        fn state(&self) -> State<Self::P1State, Self::P2State> {
            let x_count = self.0.iter().filter(|x| **x == Field::X).count();
            let o_count = self.0.iter().filter(|x| **x == Field::O).count();
            let ex = self.check_end_conditions();
            if ex == Field::X {
                State::Endgame {
                    p1_reward: 0.0,
                    p2_reward: 1.0,
                }
            } else if ex == Field::O {
                State::Endgame {
                    p1_reward: 1.0,
                    p2_reward: 0.0,
                }
            } else if x_count + o_count == 9 {
                State::Endgame {
                    p1_reward: 0.5,
                    p2_reward: 0.5,
                }
            } else if x_count < o_count {
                State::Second(*self)
            } else {
                State::First(*self)
            }
        }

        fn make_p1_move(&self, mv: &Self::P1Move) -> Result<Self, MoveError> {
            match mv {
                (r, c) if self.0[Self::coord_to_idx(*r, *c)] == Field::Empty => Ok({
                    let mut x = self.0;
                    x[Self::coord_to_idx(*r, *c)] = Field::O;
                    TicTacToe(x)
                }),
                _ => Err(MoveError::BadMove),
            }
        }

        fn make_p2_move(&self, mv: &Self::P1Move) -> Result<Self, MoveError> {
            match mv {
                (r, c) if self.0[Self::coord_to_idx(*r, *c)] == Field::Empty => Ok({
                    let mut x = self.0;
                    x[Self::coord_to_idx(*r, *c)] = Field::X;
                    TicTacToe(x)
                }),
                _ => Err(MoveError::BadMove),
            }
        }
    }
}

use mcts::*;
use ttt::*;
extern crate rand;
extern crate regex;
use rand::Rng;
use regex::Regex;

fn parse_move(str: &str) -> Option<(Coord, Coord)> {
    let format: Regex = Regex::new(r"([ABC]) ([ABC])").unwrap();
    let captures = format.captures(&str);

    fn to_c(str: &str) -> Coord {
        match str {
            "A" => Coord::A,
            "B" => Coord::B,
            "C" => Coord::C,
            _ => panic!(),
        }
    }

    captures.map(|c| {
        (
            to_c(c.get(1).unwrap().as_str()),
            to_c(c.get(2).unwrap().as_str()),
        )
    })
}

fn field_to_string(field: Field) -> char {
    match field {
        Field::X => 'X',
        Field::O => 'O',
        Field::Empty => ' ',
    }
}

fn print_board(board: &TicTacToe) {
    for c in SINGLE_COORD.iter() {
        for r in SINGLE_COORD.iter() {
            print!(
                "{:?}",
                field_to_string(board.0[TicTacToe::coord_to_idx(*r, *c)])
            );
        }
        println!();
    }
}

fn read_move() -> (Coord, Coord) {
    let mut line = String::new();
    let mut ret = None;
    while ret.is_none() {
        let _ = std::io::stdin().read_line(&mut line).unwrap();
        ret = parse_move(line.as_str());
    }
    ret.unwrap()
}

fn game_step<R: Rng>(board: TicTacToe, rng: &mut R) {
    print_board(&board);
    match board.state() {
        State::First(_) => {
            println!("O Move?");
            let mv = read_move();
            match board.make_p1_move(&mv) {
                Ok(game) => game_step(game, rng),
                Err(fail) => panic!(fail),
            }
        }
        State::Second(_) => {
            let mut mcts_node = MctsNode::new(&board);
            for _ in 0..1000 {
                mcts_iteration(board, rng, &mut mcts_node);
            }
            match mcts_node {
                MctsNode::P2Node { successors, .. } => {
                    let mv = successors
                        .iter()
                        .max_by(|x, y| {
                            (x.1.qsum_p2() / x.1.visits() as f32)
                                .partial_cmp(&(y.1.qsum_p2() / y.1.visits() as f32))
                                .unwrap()
                        })
                        .unwrap()
                        .0;
                    match board.make_p2_move(mv) {
                        Ok(game) => game_step(game, rng),
                        Err(fail) => panic!(fail),
                    }
                }
                _ => panic!(),
            }
        }
        State::Endgame {
            p1_reward,
            p2_reward,
        } => println!("Result: O: {}, X: {}", p1_reward, p2_reward),
    }
}


fn main() {
    let game = TicTacToe::new();
    let mut rng = rand::thread_rng();
    
    game_step(game, &mut rng);
}
